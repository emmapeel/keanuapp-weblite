if (!process.argv[2]) {
    console.log('Insufficient number of arguments! Need a path to sticker packs...');
}

var path = require('path'), fs = require('fs');

function fromDir(startPath, filter, callback) {

    //console.log('Starting from dir '+startPath+'/');

    if (!fs.existsSync(startPath)) {
        console.log("no dir ", startPath);
        return;
    }

    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(startPath, files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            fromDir(filename, filter, callback); //recurse
        }
        else if (filter.test(filename)) callback(filename);
    };
};

var packs = [];

fromDir(process.argv[2], /\.png$/, function (filename) {
    let file = filename.substring(process.argv[2].length);
    let parts = file.split("/");
    let pack = parts[1];
    let sticker = parts[2];
    if (!packs[pack]) {
        packs[pack] = [];
    }
    packs[pack].push(sticker);
});

var result = { baseUrl: "", packs: [] };
for (const pack of Object.keys(packs)) {
    const stickers = packs[pack];
    result.packs.push({ name: pack, stickers: stickers });
}
console.log(JSON.stringify(result, null, 2));