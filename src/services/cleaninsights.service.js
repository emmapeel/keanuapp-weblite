import { CleanInsights, ConsentRequestUi } from 'clean-insights-sdk';

export default {
    install(Vue, config) {

        class RequestUi extends ConsentRequestUi {
            showForCampaign(campaignId, campaign, complete) {
                const period = campaign.nextTotalMeasurementPeriod
                if (!period) {
                    return ''
                }
                let message = 'Help us improve!\n\n'
                    + `We would like to collect anonymous usage data between ${period.start.format('LLL')} and ${period.end.format('LLL')} to improve the quality of the product.\n\nYour help would be highly appreciated.`

                complete(window.confirm(message))
                return ''
            }
        }

        const cleanInsightsService = new Vue({
            config: config,
            data() {
                return {
                    ci: null,
                    campaignId: null,
                    requestUi: null,
                }
            },
            created() {
                if (this.$options.config) {
                    const config = this.$options.config;
                    this.ci = new CleanInsights(config);

                    // Get name of first campaign in the config.
                    this.campaignId = Object.keys(config.campaigns || { invalid: {} })[0];                

                }
            },
            methods: {
                event(category, action) {
                    if (!this.ci) {
                        return;
                    }
                    if (!this.requestUi) {
                        this.requestUi = new RequestUi();
                    }
                    this.ci.requestConsentForCampaign(this.campaignId, this.requestUi, (granted) => {
                        if (!granted) {
                            return
                        }
                        this.ci.measureEvent(category, action, this.campaignId);
                    })
                }
            }
        });
        return cleanInsightsService;
    }
}
