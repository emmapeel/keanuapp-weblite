import { ROOM_TYPE_VOICE_MODE, ROOM_TYPE_FILE_MODE, ROOM_TYPE_DEFAULT } from "../plugins/utils";

export default {
  computed: {
    availableRoomTypes() {
      let types = [{ title: this.$t("room_info.room_type_default"), description: "", value: ROOM_TYPE_DEFAULT }];
      if (this.$config.experimental_voice_mode) {
        types.push({
          title: this.$t("room_info.voice_mode"),
          description: this.$t("room_info.voice_mode_info"),
          value: ROOM_TYPE_VOICE_MODE,
        });
      }
      if (this.$config.experimental_file_mode) {
        types.push({
          title: this.$t("room_info.file_mode"),
          description: this.$t("room_info.file_mode_info"),
          value: ROOM_TYPE_FILE_MODE,
        });
      }
      return types;
    },
  },
};
