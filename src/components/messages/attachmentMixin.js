import util from "../../plugins/utils";

export default {
  data() {
    return {
      src: null,
      downloadProgress: null
    }
  },
  watch: {
    event: {
      immediate: false,
      handler(value, ignoredOldValue) {
        this.loadAttachmentSource(value);
      }
    }
  },
  mounted() {
    this.loadAttachmentSource(this.event);
  },
  beforeDestroy() {
    this.loadAttachmentSource(null); // Release
  },
  methods: {
    loadAttachmentSource(event) {
      if (this.src) {
        const objectUrl = this.src;
        this.src = null;
        URL.revokeObjectURL(objectUrl);
      }
      if (event) {
        util
        .getAttachment(this.$matrix.matrixClient, event, (progress) => {
          this.downloadProgress = progress;
          console.log("Progress: " + progress);
        })
        .then((url) => {
          this.src = url;
        })
        .catch((err) => {
          console.log("Failed to fetch attachment: ", err);
        });
  
      }
    }
  }
}